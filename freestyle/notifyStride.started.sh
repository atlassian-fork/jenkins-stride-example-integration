set +x -u

curl -X POST \
-H 'Content-Type: application/json' \
-H 'Authorization: Bearer '$STRIDE_TOKEN \
-d "{\"content\":[{\"attrs\":{\"collapsible\":true,\"context\":{\"icon\":{\"label\":\"Jenkins Build Server\",\"url\":\"https://jenkins.io/sites/default/files/jenkins_favicon.ico\"},\"text\":\"Jenkins Build Server - ${JOB_NAME} ${BUILD_DISPLAY_NAME} - BUILD STARTED\"},\"description\":{\"text\":\"Tag: ${BUILD_TAG}    Commit:${GIT_COMMIT}\"},\"details\":[{\"lozenge\":{\"appearance\":\"inprogress\",\"text\":\"BUILD STARTED\"}},{\"lozenge\":{\"appearance\":\"inprogress\",\"text\":\"${BUILD_DISPLAY_NAME}\"},\"title\":\"Build Number\"}],\"link\":{\"url\":\"${RUN_DISPLAY_URL}\"},\"text\":\"${JOB_NAME} - BUILD STARTED\",\"title\":{\"text\":\"${JOB_NAME} - BUILD STARTED\",\"user\":{\"icon\":{\"url\":\"https://jenkins.io/sites/default/files/jenkins_favicon.ico\",\"label\":\"Build Started\"}}}},\"type\":\"applicationCard\"}],\"type\":\"doc\",\"version\":1}" \
--url $STRIDE_CONVERSATION_URL
